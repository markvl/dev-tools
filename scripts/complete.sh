TOOLS="$(dirname "$(dirname "$(readlink -fm "$0")")")"

case $SHELL in
*/zsh)
    autoload bashcompinit && bashcompinit
    ;;
*)
esac

complete -C "$TOOLS/.venv/bin/aws_completer" aws
source $TOOLS/.venv/bin/az.completion.sh
