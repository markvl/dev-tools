# dev-tools

A bundle of (development) tools I use on a regular basis.

## Installation

Run:

    make

## Usage

The way I use these tools now:

In my `~/.zshenv` I have something like this:

    TOOLS="$HOME/projects/tools"
    TOOLS_BIN="$TOOLS/.venv/bin"

    if [ -d $TOOLS/scripts ] ; then
        export PATH="${PATH}:$TOOLS/scripts"
    fi

    if [ -d $TOOLS_BIN ] ; then
        export PATH="${PATH}:$TOOLS_BIN"
    fi

    # virtualenvwrapper stuff
    if [ -f $TOOLS_BIN/virtualenvwrapper.sh ]; then
        export VIRTUALENVWRAPPER_PYTHON=$TOOLS_BIN/python
        export WORKON_HOME=$HOME/projects/virtualenvs
        source $TOOLS_BIN/virtualenvwrapper.sh
    fi

    unset TOOLS

And in my section about completion I have this for AWS and Azure CLI completion:

    if [ -d $HOME/projects/tools/scripts ] ; then
        source $HOME/projects/tools/scripts/complete.sh > /dev/null
    fi
