
.PHONY: tools
tools: .venv/bin/pipenv
	.venv/bin/pipenv install

update:
	pipenv update

.venv:
	python3 -m venv .venv
	.venv/bin/pip3 install -U pip

.venv/bin/pipenv: .venv
	.venv/bin/pip3 install pipenv

.venv/bin/aws:
	@rm -rf .venv/usr/local/aws-cli   # Cleanup old install if needed
	wget --no-verbose https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip -O /tmp/awscliv2.zip
	unzip -q /tmp/awscliv2.zip -d /tmp/awscliv2_install
	/tmp/awscliv2_install/aws/install -i $$(pwd)/.venv/usr/local/aws-cli -b $$(pwd)/.venv/bin
	@rm -rf /tmp/awscliv2.zip /tmp/awscliv2_install

.PHONY: clean
clean:
	rm -rf .venv
